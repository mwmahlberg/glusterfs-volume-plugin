GIT_HASH=$(shell git rev-parse --short HEAD)

.PHONY: integration-test clean dist-clean vendor remove-plugin

docker-plugin: docker-image remove-plugin
	docker create --name vp-build-$(GIT_HASH) mwmahlberg/glusterfs-volume-plugin
	mkdir -p build/plugin/rootfs
	docker export vp-build-$(GIT_HASH) | tar -x -C build/plugin/rootfs
	docker rm vp-build-$(GIT_HASH)
	cp config.json ./build/plugin
	docker plugin create mwmahlberg/glusterfs-volume-plugin:latest ./build/plugin

remove-plugin:
	docker plugin rm -f mwmahlberg/glusterfs-volume-plugin || true

glusterfs-volume-plugin: clean vendor
	go build -v

glusterfs-volume-plugin.linux: vendor
	 GOOS=linux go build -v -o glusterfs-volume-plugin.linux

vendor: $(GOPATH)/bin/dep
	dep ensure

$(GOPATH)/bin/dep:
	curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

integration-test: build-linux
	vagrant up

clean:
	$(RM) -r build
	$(RM) glusterfs-volume-plugin*

dist-clean: clean
	$(RM) -rf vendor
	vagrant destroy -f

docker-image: glusterfs-volume-plugin.linux
	docker build -t mwmahlberg/glusterfs-volume-plugin -f Dockerfile.plugin .
	

