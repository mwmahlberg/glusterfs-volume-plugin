package main

import (
	"fmt"
	"strings"

	"github.com/asaskevich/govalidator"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

// HostListValue is a kingpin.Value.
type HostListValue []string

// Set takes a string, splits it by comma and sets the value.
func (l *HostListValue) Set(value string) error {
	for _, p := range strings.Split(value, ",") {
		p = strings.TrimSpace(p)
		if !govalidator.IsHost(p) {
			return fmt.Errorf("'%s' is not a valid hostname or IP", p)
		}
		*l = append(*l, p)
	}
	return nil
}

func (l *HostListValue) String() string {
	return fmt.Sprintf("%s", *l)
}

// HostList is wrapper to use a custom parser for kingpin.
func HostList(s kingpin.Settings) *HostListValue {
	l := make(HostListValue, 0)
	s.SetValue((*HostListValue)(&l))
	return (*HostListValue)(&l)
}
