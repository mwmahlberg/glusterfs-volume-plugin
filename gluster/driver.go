package gluster

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"github.com/docker/go-plugins-helpers/volume"
)

var basepath = filepath.Join(volume.DefaultDockerRootDirectory, "glusterfs")

// Driver is an implementation of a Docker volume plugin driver for glusterfs.
type Driver struct {
	hosts []string
	debug bool
}

// NewDriver returns a volume.Driver for GlusterFS.
func NewDriver(hosts []string, debug bool) (volume.Driver, error) {

	if len(hosts) < 1 {
		return nil, errors.New("At least one host of trusted pool required")
	}
	return Driver{hosts: hosts, debug: debug}, nil
}

// Create implements volume.Driver.Create .
// It is called when a docker volume is implicitly
// or explicitly created. Note, however, that this only
// creates the required mountpoint and does not mount it.
func (d Driver) Create(req *volume.CreateRequest) error {
	log.Printf("Creating volume '%s", req.Name)
	p := volumePath(req.Name)
	for k, v := range req.Options {
		log.Println(k, v)
	}
	_, err := os.Stat(p)
	switch {
	case os.IsNotExist(err):
		if cerr := os.MkdirAll(p, 0750); cerr != nil {
			log.Printf("Volume %s: Could not create mount point '%s': %s", req.Name, p, cerr)
			return cerr
		}
	case os.IsPermission(err):
		log.Printf("Volume %s: Insufficient permissions to create mountpoint '%s': %s", req.Name, p, err)
		return err
	}

	return nil
}

// List returns all docker volumes which are managed by this driver.
// The information is used when "docker volume ls" is called.
func (d Driver) List() (*volume.ListResponse, error) {
	r := &volume.ListResponse{}
	if d.debug {
		log.Printf("Listing basepath '%s'", basepath)
	}
	entries, err := ioutil.ReadDir(basepath)
	switch {
	case os.IsNotExist(err):
		log.Printf("Basepath '%s' does not exist!", basepath)
		return nil, err
	case os.IsPermission(err):
		log.Printf("Insufficient permissions to inspect mountpoints under '%s': %s", basepath, err)

		return nil, err
	}

	var vols []*volume.Volume
	for _, e := range entries {
		if d.debug {
			log.Printf("Processing entry '%s'", volumePath(e.Name()))
		}
		if e.IsDir() {
			if d.debug {
				log.Printf("Entry '%s' is an directory", volumePath(e.Name()))
			}
			v := &volume.Volume{
				Mountpoint: volumePath(e.Name()),
				Name:       filepath.Base(e.Name()),
			}

			vols = append(vols, v)
		}
	}
	r.Volumes = vols
	return r, nil
}

// Get implements the Get(*volume.GetRequest) function of the docker volume plugin interface.
// It provides the information for "docker volume inspect <volumename>".
// Furthermore, it is called various times by the handler to get information
// about a volume during its lifecycle.
func (d Driver) Get(req *volume.GetRequest) (*volume.GetResponse, error) {
	p := volumePath(req.Name)

	if _, err := os.Stat(p); err != nil {
		return nil, err
	}

	m, err := mounted(p)
	if err != nil {
		log.Printf("Volume %s: Unable to determine mount state of mountpoint '%s': %s", req.Name, p, err)
		return nil, err
	}

	s := make(map[string]interface{})
	s["mounted"] = fmt.Sprintf("%t", m)

	r := &volume.GetResponse{}
	r.Volume = &volume.Volume{Mountpoint: p, Name: req.Name, Status: s}

	return r, nil
}

// Remove implements the Remove() function of the docker volume plugin interface.
func (d Driver) Remove(req *volume.RemoveRequest) error {

	p := volumePath(req.Name)

	m, merr := mounted(p)
	switch {
	case os.IsNotExist(merr):
		log.Printf("Volume %s: Mount directory '%s' does not exist: %s", req.Name, p, merr)
		return merr
	case os.IsPermission(merr):
		log.Printf("Volume %s: Insufficient permissions to inspect mountpoint '%s': %s", req.Name, p, merr)
		return merr
	case m:
		log.Printf("Volume %s: Directory to remove ('%s') has a filesystem mounted! Trying to unmount...", req.Name, p)
		if err := d.Unmount(&volume.UnmountRequest{Name: req.Name}); err != nil {
			log.Printf("Volume %s: Could not unmount Glusterfs volume on '%s': %s", req.Name, p, err)
			return err
		}
		log.Printf("Volume '%s': Unmounted '%s'", req.Name, p)
	}

	err := os.Remove(p)
	switch {
	case os.IsNotExist(err):
		log.Printf("Volume %s: Could not remove directory '%s': %s", req.Name, p, err)
		return err
	case os.IsPermission(err):
		log.Printf("Volume %s: Insufficient permissions to inspect mountpoint '%s': %s", req.Name, p, err)
		return err
	case err != nil:
		log.Printf("Volume %s: Error while removing mountpoint '%s': %s", req.Name, p, err)
		return err
	}
	log.Printf("Volume %s: Successfully removed volume and mountpoint '%s'", req.Name, p)

	return nil
}

// Path implements github.com/docker/go-plugins-helpers/volume.Path .
func (d Driver) Path(req *volume.PathRequest) (*volume.PathResponse, error) {
	return &volume.PathResponse{Mountpoint: volumePath(req.Name)}, nil
}

// Mount implements the Remove(*volume.MountRequest) function of the docker volume plugin interface.
func (d Driver) Mount(req *volume.MountRequest) (*volume.MountResponse, error) {
	p := volumePath(req.Name)
	m, _ := mounted(p)
	if !m {
		log.Printf("Volume %s: Mounting gluster volume '%s:/%s' to mountpoint '%s'", req.Name, d.hosts, req.Name, p)
		var src = fmt.Sprintf("%s:/%s", strings.Join(d.hosts, ","), req.Name)
		log.Printf("\t%s", src)

		var out bytes.Buffer
		var mountCmd = exec.Command("mount.glusterfs",
			src, p,
			"-o", "noatime",
		)
		done := make(chan error)
		mountCmd.Stderr = &out

		log.Println("Mounting...")

		mountCmd.Start()
		go func() {
			done <- mountCmd.Wait()
		}()

		timeout := time.After(5 * time.Second)

		select {
		case <-timeout:
			log.Printf("Mount of volume %s from %s timed out.", req.Name, src)
			mountCmd.Process.Kill()
			return nil, errors.New("volume-glusterfs: operation timed out")
		case err := <-done:
			if err != nil {
				log.Printf("Error mounting '%s' to '%s': %s\n\t%s",
					src, p, err, out.String())
				return nil, errors.New("volume-glusterfs: " + err.Error())
			}
			mountCmd.Process.Release()
		}

		log.Println("Mounted!")
	}
	return &volume.MountResponse{Mountpoint: p}, nil
}

// Unmount implements the Unmount(*volume.UnmountRequest) function of the docker volume plugin interface.
func (d Driver) Unmount(req *volume.UnmountRequest) error {

	// Generate path to mounted gluster volume inside runc container
	p := volumePath(req.Name)

	// We only need to unmount if gluster volume is mounted.
	m, err := mounted(p)

	if err != nil {
		log.Printf("Volume %s: Unable to verify status of mountpoint '%s': %s", req.Name, p, err)
		return err
	}

	if m {
		var umountCmd = exec.Command("umount", "-f", p)
		var out bytes.Buffer
		umountCmd.Stderr = &out

		done := make(chan error)
		umountCmd.Start()
		go func() {
			done <- umountCmd.Wait()
		}()

		timeout := time.After(5 * time.Second)

		select {
		case <-timeout:
			log.Printf("Umount of volume '%s' timed out", p)
			return errors.New("volume-glusterfs: operation timed out")
		case err := <-done:
			if err != nil {
				log.Printf("Error unmounting '%s': %s", p, err)
				return errors.New("volume-glusterfs: " + err.Error())
			}
		}
	}
	return nil
}

// Capabilities implements the Capabilities() function of the docker volume plugin interface.
func (d Driver) Capabilities() *volume.CapabilitiesResponse {
	r := &volume.CapabilitiesResponse{}
	r.Capabilities.Scope = "local"
	return r
}

func (d Driver) mount(name string) error {

	return nil
}
