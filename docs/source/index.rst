.. glusterfs-volume-plugin documentation master file, created by
   sphinx-quickstart on Wed Jul  4 22:20:41 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

glusterfs-volume-plugin
=======================

.. toctree::
   :numbered:
   :maxdepth: 3
   :caption: Contents:

   intro
   install
   use
   test
   license


fooo
----

bar
---


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
