package main

import (
	"log"
	"os"

	"bitbucket.org/mwmahlberg/glusterfs-volume-plugin/gluster"
	"github.com/docker/go-plugins-helpers/volume"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

var (
	debug   = kingpin.Flag("debug", "Enable debug mode.").Default("false").Envar("GFSP_DEBUG").Bool()
	hosts   = HostList(kingpin.Flag("hosts", "GlusterFS hosts").Default("127.0.0.1").Envar("GFSP_HOSTS"))
	version = "0.2.1"
)

func init() {
	log.SetPrefix("[Gluster Volume Plugin] ")
	log.SetFlags(0)
	log.SetOutput(os.Stdout)
}

func main() {
	kingpin.Version(version)
	kingpin.Parse()

	log.Println("Setting up driver")
	d, err := gluster.NewDriver(*hosts, *debug)

	if err != nil {
		log.Fatalf("Error setting up driver (GFSP_HOSTS: %s, GFSP_DEBUG:%b): %s ", hosts, debug, err)
	}

	log.Println("Setting up handler")
	h := volume.NewHandler(d)

	log.Println("Starting handler")
	herr := h.ServeUnix("gfsp", 0)
	if herr != nil {
		log.Printf("Error starting handler: %s", herr)
		os.Exit(1)
	}
	log.Println("Stopped handler")
}
