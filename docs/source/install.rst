Installation & Configuration
============================

.. contents:: :local:

.. index:: install


You can either use glusterfs-volume-plugin as a `docker engine managed plugin <https://docs.docker.com/engine/extend/>`_
or a legacy plugin.

In short, plugins managed by the docker engine are special containers which are
started before any containers are started. They are be managed via the
`docker plugin command <docker_plugin>`_.

.. Important:: It is strongly recommended to use the docker managed plugin.

Legacy plugins are processes which
need to be managed by the system administrator.

Docker managed plugin
---------------------

.. note:: The docker managed plugins are available as of docker 1.13

Install the plugin
..................

The installation is pretty straightforward.


   .. code-block:: console

      $ docker plugin install mwmahlberg/glusterfs-volume-plugin
      Plugin "mwmahlberg/glusterfs-volume-plugin" is requesting the following privileges:
       - network: [host]
       - mount: [/etc/resolv.conf]
       - device: [/dev/fuse]
       - capabilities: [CAP_SYS_ADMIN]
      Do you grant the above permissions? [y/N]

Press :kbd:`y` and :kbd:`<Enter>`.

    .. code-block:: console

       Trying to pull repository docker.io/mwmahlberg/glusterfs-volume-plugin ...
       latest: Pulling from docker.io/mwmahlberg/glusterfs-volume-plugin
       28d15fbb676f: Download complete
       Digest: sha256:386ce0d6b514a74e10a8f891f5dca8d247a46b150909b25c95ba6e330d034b18
       Status: Downloaded newer image for docker.io/mwmahlberg/glusterfs-volume-plugin:latest
       Installed plugin mwmahlberg/glusterfs-volume-plugin


.. note:: The sha256 hashes you may see can vary, depending on the current version of the plugin.


Now that the plugin is installed, we should :ref:`configure <configuration_m>` it.


.. _configuration_m:

Configure the plugin
....................

+------------+---------------+------------------------------------+
| Name       | default value | meaning                            |
+============+===============+====================================+
| GFSP_HOSTS | 127.0.0.1     | Comma separated list of            |
|            |               | trusted pool members to connect to |
+------------+---------------+------------------------------------+
| GFSP_DEBUG | false         | Log debug information              |
+------------+---------------+------------------------------------+

.. code-block:: console

   $ docker plugin set mwmahlberg/glusterfs-volume-plugin GFSP_HOSTS="192.168.0.2,my-glusterd-host"

Activate the plugin
...................

.. code-block:: console

   $ docker plugin enable mwmahlberg/glusterfs-volume-plugin


Legacy plugin
-------------------------------

.. note:: Installation instructions are only provided for systemd-based GNU/Linux systems.
          Please feel free to open a pull request if you can provide instructions for
          upstart / SYSV / openrc systems.

Install the legacy plugin
.........................

.. code-block:: console

   $ cd /tmp
   $ curl -L https://bitbucket.org/mwmahlberg/glusterfs-volume-plugin/downloads/glusterfs-volume-plugin-master.tar.gz | tar -xvzf -
   $ intall -m 0640 contrib/systemd/* /etc/systemd/system/
   $ install -m 0750 -o root -g root glusterfs-volume-plugin /usr/local/sbin/
   $ systemctl daemon-reload

.. _configuration_l:

Configure the legacy plugin
...........................

Create an ``overide.conf`` for the glusterfs-volume-plugin.service:

   .. code-block:: console

      $ systemctl edit glusterfs-volume-plugin.service

And editor will open. Please edit the contents according to the example below and save.

   .. code-block:: console

      [Service]
      Environment=GFSP_HOSTS="your-first-glusterd,your-second-glusterd"

Now we can activate the legacy plugin.

Activate the legacy plugin
..........................

.. code-block:: none

   $ systemctl enable glusterfs-volume-plugin.service
   $ systemctl enable glusterfs-volume-plugin.socket
   $ systemctl start glusterfs-volume-plugin.socket
   $ systemctl restart docker

.. _docker_plugin: https://docs.docker.com/engine/reference/commandline/plugin/
.. _downloads: https://bitbucket.org/mwmahlberg/glusterfs-volume-plugin/downloads/
