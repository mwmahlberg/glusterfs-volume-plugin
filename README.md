[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg?style=flat-square)](https://opensource.org/licenses/Apache-2.0)
[![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/mwmahlberg/glusterfs-volume-plugin.svg?style=flat-square)](https://bitbucket.org/mwmahlberg/glusterfs-volume-plugin/addon/pipelines/home#!/)
[![Documentation Status](https://readthedocs.org/projects/glusterfs-volume-plugin/badge/?version=latest&style=flat-square)](http://glusterfs-volume-plugin.readthedocs.io)
[![Bitbucket issues](https://img.shields.io/bitbucket/issues/mwmahlberg/glusterfs-volume-plugin.svg?style=flat-square)](https://Bitbucket.org/mwmahlberg/glusterfs-volume-plugin/issues/)

[![Amazon whishlist 12 items, none bought](https://img.shields.io/badge/amazon_wishlist-12_items-red.svg?style=flat-square)](https://www.amazon.de/registry/wishlist/22DJ9Y2OV3ER9)

# glusterfs-volume-plugin

A plugin for docker to use glusterfs volumes.

## Documentation

You can find the documentation at http://glusterfs-volume-plugin.readthedocs.io

## Bug fixes et al

I will do regular bug fixes and improvements. However, if you wish a change or a bug fix urgently,
it might help if you buy somthing of my [Amazon wishlist](https://www.amazon.de/registry/wishlist/22DJ9Y2OV3ER9) ;)
