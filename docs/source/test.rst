.. _testdrive:

Try it out!
===========

If you want to :ref:`test it <testdrive>` the plugin: A `Vagrantfile <vagrant_>`_ is provided.

  .. code-block:: shell

     $ curl TODO
     $ vagrant up
     [...]
     $ vagrant ssh node

Now that you have a test environment, you can continue to :ref:`configure the glusterfs-volume-plugin <configuration_m>`.

.. _vagrant: https://www.vagrantup.com
